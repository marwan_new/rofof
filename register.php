<?php include('header.php'); ?>
				
<section class="page-header page-header-light page-header-more-padding">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<h1>حساب جديد</h1>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<!-- <h4 class="heading-primary mt-lg">حساب جديد</h4> -->
			<!-- <p>قم بالتسجيل الأن واحصل على عروض قيمة</p> -->
			<form id="contactForm" action="index.php" method="POST" >
				<input type="hidden" value="Contact Form" name="subject" id="subject">
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label>الاسم الشخصي *</label>
							<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label>البريد الإلكتروني *</label>
							<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label>كلمة المرور *</label>
							<input type="password" class="form-control" name="password" id="password" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input type="submit" value="حساب جديد" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
					</div>
				</div>

				مسجل بالفعل
				<a href="login.php" class="btn btn-primary">تسجيل الدخول </a>
			</form>
		</div>
		
		
		<div class="col-md-6">
					<img  class="img-responsive " style="margin-top: 50px;" src="img/facebook-sign-in.png">

		</div>
	</div>
</div>
<div id="googlemaps" class="google-map mt-xlg mb-none"></div>
<?php include('footer.php'); ?>
