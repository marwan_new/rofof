<?php include('header.php'); ?>
				
<section class="page-header page-header-light page-header-more-padding">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<h1>اتصل بنا</h1>
			</div>
		</div>
	</div>
</section>

<div class="container-fluid">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3451.3651487208385!2d31.398100614702546!3d30.11236318185765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145817082ea84f41%3A0xe065af2151e21386!2sCairo+International+Airport!5e0!3m2!1sen!2snl!4v1521437904587" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<div class="container-fluid padd20">
<div class="container">
	<div class="row">
		<div class="col-md-12">
		<div class="col-md-8">
			<h4 class="heading-primary mt-lg">أرسل رسالة</h4>
			<p>سنقوم بالرد عليك  باقرب وقت ممكن </p>

			<div class="alert alert-success hidden mt-lg" id="contactSuccess">
				<strong>Success!</strong> Your message has been sent to us.
			</div>

			<div class="alert alert-danger hidden mt-lg" id="contactError">
				<strong>Error!</strong> There was an error sending your message.
				<span class="font-size-xs mt-sm display-block" id="mailErrorMessage"></span>
			</div>

			<form id="contactForm" action="contact-form.php" method="POST" >
				<input type="hidden" value="Contact Form" name="subject" id="subject">
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label>الاسم الشخصي *</label>
							<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label>البريد الإلكتروني *</label>
							<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label>الموضوع *</label>
							<input type="text" value="" class="form-control" name="subject" id="subject">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label>الرسالة *</label>
							<textarea maxlength="5000" data-msg-required="Please enter your message." rows="5" class="form-control" name="message" id="message" required></textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input type="submit" value="أرسل الرسالة" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-4">
			<h4 class="heading-primary mt-lg">بيانات الاتصال</h4>
			<ul class="list list-icons mt-md">
				<li>
					<a href="#">
						<i class="icon-map icons"></i> 24 ش السعادة متفرع من شارع النصر - ميدان البرج - البحيرة
					</a>
				</li>
				<li>
					<a href="mailto: mail@domain.com">
						<i class="icon-envelope-open icons"></i> mail@domain.com
					</a>
				</li>
				<li>
					<a href="#">
						<i class="icon-call-out icons"></i> (0966) 123-456-789
					</a>
				</li>
				<li>
					<a href="#">
						<i class="icon-social-linkedin icons"></i> Lindekin
					</a>
				</li>
				<li>
					<a href="#">
						<i class="icon-social-facebook icons"></i> Facebook
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<?php include('footer.php'); ?>
