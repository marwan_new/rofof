<?php include('header.php'); ?>

<section class="page-header page-header-light page-header-more-padding">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<h1>التصنيفات</h1>
			</div>
		</div>
	</div>
</section>

<div class="container-fluid bricks_bg padd20">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row hp">
					<ul id="listingLoadMoreWrapper" class="properties-listing sort-destination p-none" data-total-pages="2">
						<?php for($i=1; $i<10; $i++): ?>
						<li class="col-md-4 col-sm-6 col-xs-12 listing-item text-center">
							<div class="row">
								<div class="col-xs-3">
									<a href="ads.php">
										<h2 class="hp_shielve_head">أحدث العروض</h2>
									</a>
								</div>
								<div class="col-xs-9">
									<div class="owl-carousel hp_prods_slider">
									<?php for($j=1; $j<10; $j++): ?>
										<div class="hp_prod">
											<a href="ad_details.php?id=<?=$j?>">
												<div class="cat_icon hp_prod_img"><img src="img/cats/<?=$i?>.jpg"></div>
												<div class="hp_price">40 SR</div>
											</a>
										</div>
									<?php endfor; ?>
									</div>	
								</div>
							</div>
						</li>
						<?php endfor; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include"footer.php"; ?>