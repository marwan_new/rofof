<?php include"header.php"; ?>
<section class="page-header page-header-light page-header-more-padding" style="padding-bottom: 0">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1 class="text-warning">كل العروض</h1>
			</div>
		</div>
		<div class="row mt-lg">
			<div class="col-md-12">
				<form action="ads.php" method="POST" class="text-center">
					<div class="col-md-3">
						<div class="form-control-custom mb-md">
							<input type="text" class="form-control" id="search_q" placeholder="ابحث في العروض والماركات">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-control-custom mb-md">
							<select class="form-control">
								<option>اختر الشركة أو المحل</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-control-custom mb-md">
							<select class="form-control">
								<option>اختر التصنيف</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<input type="submit" value="ابحث الأن" class="btn btn-primary btn-block">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<div class="bricks_bg padd20">
	<div class="container">
		<div class="row mb-lg">
			<ul class="properties-listing sort-destination p-none">
				<?php for($i=1; $i<=8; $i++): ?>
				<li class="col-md-3 col-sm-6 col-xs-6 p-md">
					<div class="cat_prod">
						<a href="ad_details.php?id=<?=$i?>">
							<div class="cat_prod_img">
								<img src="img/cats/<?=$i?>.jpg">
								<div class="cat_price">40 USD</div>
							</div>
							<div class="sales_on">خصم <strong>10%</strong></div>
						</a>
					</div>

					<!-- <div class="listing-item hidden">
						<a href="ad_details.php" class="text-decoration-none">
							<span class="thumb-info thumb-info-lighten">
								<span class="thumb-info-wrapper m-none">
									<img src="img/offers/<?=$i?>.jpg" class="img-responsive" alt="">
									<span class="thumb-info-listing-type background-color-secondary text-uppercase text-color-light font-weight-semibold p-xs pl-md pr-md">
										خصم 20%
									</span>
								</span>
								<span class="thumb-info-price background-color-primary text-color-light p-sm pl-md pr-md">
									خصم 20% على جميع أنواع العطور من محل هكاظ
								</span>
								<span class="custom-thumb-info-title b-normal p-lg">
									<ul class="accommodations text-uppercase font-weight-bold p-none text-sm">
										<li>
											<span class="accomodation-title">
												تاريخ العرض:
											</span>
											<span class="accomodation-value custom-color-1">
												2 مارس 2018
											</span>
										</li>
										<li>
											<span class="accomodation-title">
												الماركة: 
											</span>
											<span class="accomodation-value custom-color-1">
												هوجو
											</span>
										</li>
										<li>
											<span class="accomodation-title">
												محل: 
											</span>
											<span class="accomodation-value custom-color-1">
												عكاظ
											</span>
										</li>
									</ul>
								</span>
							</span>
						</a>
					</div> -->
				</li>
				<?php endfor; ?>
			</ul>
		</div>
		<div class="row mt-lg mb-xlg">
			<div class="col-md-12 center">
				<ul class="pagination">
					<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
					<li class="active"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php include('footer.php'); ?>