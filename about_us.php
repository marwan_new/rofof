<?php include"header.php"; ?>
				
<section class="page-header page-header-light page-header-more-padding">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<h1>من نحن</h1>
			</div>
		</div>
	</div>
</section>

<div class="container padd20">
	<div class="row pb-xl pt-md">
		
		
		<div class="col-md-12">			

			<div class="row">
				<div class="col-md-12">
					<img  class="img-responsive " style="margin: 0px auto;" src="img/slide1.jpg">
				</div>
				
				<div class="col-md-12">
					<h4 class="mt-xlg mb-md">مهمتنا</h4>

					<p>
						هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء.
					</p>
				</div>
				<div class="col-md-12">
					<h4 class="mt-xlg mb-md">رؤيتنا</h4>

					<p>
						 كلمات أو عبارات محرجة أو غير لائقة مخبأة في هذا النص. بينما تعمل جميع مولّدات نصوص لوريم إيبسوم على الإنترنت على إعادة تكرار مقاطع من نص لوريم إيبسوم نفسه عدة مرات بما تتطلبه الحاجة، يقوم مولّدنا هذا باستخدام كلمات من قاموس يحوي على أكثر من 200 كلمة لا تينية، مضاف إليها مجموعة من الجمل النموذجية، لتكوين نص لوريم إيبسوم ذو شكل منطقي قريب إلى النص الحقيقي.
					</p>
				</div>
				<div class="col-md-12">
					<h4 class="mt-xlg mb-md">الأهداف</h4>

					<p>
						 بروفيسور اللغة اللاتينية في جامعة هامبدن-سيدني في فيرجينيا بالبحث عن أصول كلمة لاتينية غامضة في نص لوريم إيبسوم وهي "consectetur"، وخلال تتبعه لهذه الكلمة في الأدب اللاتيني اكتشف المصدر الغير قابل للشك. فلقد اتضح أن كلمات نص لوريم إيبسوم تأتي من الأقسام 1.10.32 و 1.10.33 من كتاب "حول أقاصي الخير والشر
					</p>
				</div>
			</div>
			
			
		</div>
	</div>
</div>

<?php include"footer.php"; ?>