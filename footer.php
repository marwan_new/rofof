		<footer id="footer" class="hidden-xs">
			<div class="container">
				<div class="navbar">
					<div class="container-fluid text-center">
						<ul class="nav navbar-nav">
							<li class="active"><a href="#">الرئيسية</a></li>
							<li><a href="#">العروض</a></li>
							<li><a href="#">شركات</a></li>
							<li><a href="#">تصنيفات</a></li>
							<li><a href="#">مفضلة</a></li>
							<li><a href="#">مطاعم وحلويات</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="footer-copyright pb-none">
				<div class="container text-center">
					<div class="row pt-md pb-md">
						<div class="col-md-12 m-none">
							<p>© 2018 رفوف - كافة الحقوق محفوظة</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
</div>
<footer class="mobile_nav">
	<div class="navbar_footer">
		<ul class="nav_list">
			<li><a href="ads.php"><i class="fa fa-gift"></i> العروض</a></li>
			<li><a href="companies.php"><i class="fa fa-th"></i> شركات</a></li>
			<li><a href="cats.php"><i class="fa fa-building"></i> تصنيفات</a></li>
			<li><a href="favorites.php"><i class="fa fa-star"></i> مفضلة</a></li>
			<li><a href="contact.php"><i class="fa fa-ellipsis-h"></i> المزيد</a></li>
		</ul>
	</div>
</footer>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>
<script src="js/theme.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/demos/demo-real-estate.js"></script>
<script src="js/custom.js"></script>
<script src="js/theme.init.js"></script>
</body></html>