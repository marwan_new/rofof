<?php include('header.php'); ?>
<div class="slider-container light rev_slider_wrapper" style="height: 600px;">
	<div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 600, 'disableProgressBar': 'on', 'navigation': {'arrows': {'enable': true, 'left':{'container':'slider','h_align':'right','v_align':'center','h_offset':20,'v_offset':-80},'right':{'container':'slider','h_align':'right','v_align':'center','h_offset':20,'v_offset':80}}}}">
		<div class="slides-number hidden-xs">
			<span class="atual">1</span>
			<span class="total">6</span>
		</div>
		<ul>
			<?php for($i=1; $i<4; $i++): ?>
			<li data-transition="slide">
				<img src="img/slide<?=$i?>.jpg"  
					alt=""
					data-bgposition="center center" 
					data-bgfit="cover" 
					data-bgrepeat="no-repeat"
					class="rev-slidebg">

				<div class="tp-caption tp-shape tp-shapewrapper tp-resizeme skrollable skrollable-after" 
					id="slide-529-layer-1" 
					data-x="left" data-hoffset="15" data-y="center" data-voffset="0" data-width="360" 
					data-height="360" data-whitespace="nowrap" 	data-transform_idle="o:1;" 
					data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" 
					data-transform_out="x:left;s:1200;e:Power3.easeInOut;" 
					data-start="500" data-responsive_offset="on" 
					style="background-color: rgb(255, 255, 255); padding: 30px; overflow: hidden;">
						<span class="featured-border" style="border: 2px solid #dcdde0; width: 90%; position: absolute; height: 90%; top: 5%; left: 5%;"></span>
						<span class="feature-tag" data-width="50" data-height="50" style="background: #8c3b10; color: #FFF;padding: 15px 102px; position: absolute; right: -24%; top: 6%; -webkit-transform: rotate(45deg); -moz-transform: rotate(45deg); -ms-transform: rotate(45deg); -o-transform: rotate(45deg); transform: rotate(45deg);">
							خصومات 50%
						</span>
					</div>
				<div class="tp-caption main-label"
					data-x="left" data-hoffset="35"	data-y="center" data-voffset="-50" data-start="1500"
					data-whitespace="nowrap" data-transform_in="y:[-100%];s:500;" data-transform_out="opacity:0;s:500;"
					data-textAlign="center" 
					style="z-index: 5; font-size: 25px; color: #000; text-transform: uppercase; font-weight: 900; text-shadow: none; width: 27vw; max-width: 320px;"
					data-mask_in="x:0px;y:0px;">أفضل عروض</div>
				<div class="tp-caption"
					data-x="left" data-hoffset="35" data-y="center" data-voffset="0" data-start="1500"
					data-height="44" data-whitespace="nowrap" data-transform_idle="o:1;" 
					data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" 
					data-transform_out="x:left;s:1200;e:Power3.easeInOut;"
					data-textAlign="center" 
					style="z-index: 5; font-size: 40px; font-weight: 400; text-transform: uppercase; color: #219cd2; line-height: 0.8em; width: 27vw; max-width: 320px;"
					data-mask_in="x:0px;y:0px;">الأجهزة المنزلية</div>
				<a class="tp-caption slide-button" href="ad_details.php" 
					data-x="left" data-hoffset="108" data-y="center" data-voffset="60" data-start="1500"
					data-whitespace="nowrap" data-transform_in="y:[100%];s:500;" data-transform_out="opacity:0;s:500;"
					style="z-index: 5; font-size: 1em; text-transform: uppercase; background: #e21e1e; padding: 12px 35px; color: #FFF;"
					data-mask_in="x:0px;y:0px;">تسوق الأن</a>
			</li>
			<?php endfor; ?>
		</ul>
	</div>
</div>


<div class="container-fluid bricks_bg">
	
		<div class="owl-carousel ads_slider">
		<?php for($j=1; $j<4; $j++): ?>
			<div class="hp_prod">
				<a href="ad_details.php?id=<?=$j?>">
					<div class="ad_img"><img src="img/slide<?=$j?>.jpg"></div>
				</a>
			</div>
		<?php endfor; ?>
		</div>
	
	<div class="container">
		<div class="row pt-xlg">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12 text-center">
						<h2 class="font-weight-normal mb-xs hidden">
							كل ما تحتاجه <strong class="text-color-secondary font-weight-extra-bold">تجده</strong>
							<strong class="text-color-secondary font-weight-extra-bold">هنا</strong>
						</h2>
					</div>
				</div>
				<div class="row hp">
					<ul id="listingLoadMoreWrapper" class="properties-listing sort-destination p-none" data-total-pages="2">
						<?php for($i=1; $i<10; $i++): ?>
						<li class="col-md-4 col-sm-6 col-xs-12 listing-item text-center">
							<div class="row">
								<div class="col-xs-3">
									<a href="ads.php">
										<h2 class="hp_shielve_head">أحدث العروض</h2>
									</a>
								</div>
								<div class="col-xs-9">
									<div class="owl-carousel hp_prods_slider">
									<?php for($j=1; $j<10; $j++): ?>
										<div class="hp_prod">
											<a href="ad_details.php?id=<?=$j?>">
												<div class="cat_icon hp_prod_img"><img src="img/cats/<?=$i?>.jpg"></div>
												<div class="hp_price">40 SR</div>
											</a>
										</div>
									<?php endfor; ?>
									</div>
								</div>
							</div>
						</li>
						<?php endfor; ?>
					</ul>
					<div class="col-md-12 text-center">
						<a href="ads.php" class="btn btn-warning btn-xs font-size-md outline-none p-md pl-xlg pr-xlg m-auto mb-xlg mt-xlg">كل العروض</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include"footer.php"; ?>