<?php include('header.php'); ?>
				
<section class="page-header page-header-light page-header-more-padding">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<h1>تسجيل الدخول</h1>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<!-- <h4 class="heading-primary mt-lg">تسجيل الدخول</h4> -->
			<p>قم بالتسجيل الأن واحصل على عروض قيمة</p>
			<form id="contactForm" action="index.php" method="POST" >
				<input type="hidden" value="Contact Form" name="subject" id="subject">
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label>البريد الإلكتروني *</label>
							<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email=" الرجاء ادخال بريد الكترونى  صحيح" maxlength="100" class="form-control" name="email" id="email" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label>كلمة المرور *</label>
							<input type="password" class="form-control" name="password" id="password" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input type="submit" value="تسجيل الدخول" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
					</div>
				</div>

				ليس لديك حساب
				<a href="register.php" class="btn btn-primary">سجل الأن </a>
			</form>
		</div>
		<div class="col-md-6 text-center _hidden-xs">
		<img  class="img-responsive " style="margin-top: 50px;" src="img/facebook-sign-in.png">

		
		</div>
	</div>
</div>
<div id="googlemaps" class="google-map mt-xlg mb-none"></div>
<?php include('footer.php'); ?>
