<?php include('header.php'); ?>
				
<section class="page-header page-header-light page-header-more-padding">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<h1>الشركات والمطاعم</h1>
			</div>
		</div>
	</div>
</section>

<div class="bricks_bg padd20">
	<div class="container">
		<?php for($i=0; $i<3; $i++): ?>
		<div class="agent-item">
			<div class="row">
				<div class="col-md-2">
					<img src="img/icon.jpg" class="co_img" />
				</div>
				<div class="col-md-6">
					<h3 class="mt-xs mb-xs">مطعم حضرموت العربي</h3>
					<h6 class="mb-xs"><strong class="text-info">قسم:</strong> مطاعم</h6>				
					<p>
						نبذة عن المطعم هنا في هذا الجزء نتابي نت نبذة عن المطعم هنا في هذا الجزء نتابي نت نبذة عن المطعم هنا في 
					</p>
					<a class="btn btn-secondary btn-sm mt-md" href="company_details.php">عرض التفاصيل</a>
					<a class="btn btn-primary btn-sm mt-md" href="ads.php">عروض المطعم</a>
				</div>
				<div class="col-md-4">
					<ul class="list list-icons m-sm ml-xl">
						<li>
							<a href="mailto: mail@domain.com">
								<i class="icon-envelope-open icons"></i> mail@domain.com
							</a>
						</li>
						<li>
							<a href="#">
								<i class="icon-call-out icons"></i> (0966) 123-4567-789
							</a>
						</li>
						<li>
							<a href="#">
								<i class="icon-social-facebook icons text-primary"></i> Facebook
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<?php endfor; ?>
	</div>
</div>
<?php include('footer.php'); ?>