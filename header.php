<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	
	<title>ROFOF</title>
	<meta name="keywords" content="Rofof template" />
	<meta name="description" content="Rofof template">
	<meta name="author" content="rofof.com">
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/bootstrap-rtl/bootstrap-rtl.css">
	<link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="vendor/animate/animate.min.css">
	<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">
	<link rel="stylesheet" href="css/theme.css">
	<link rel="stylesheet" href="css/theme-elements.css">
	<link rel="stylesheet" href="css/theme-blog.css">
	<link rel="stylesheet" href="css/theme-shop.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
	<link rel="stylesheet" href="css/skins/skin-real-estate.css"> 
	<link rel="stylesheet" href="css/demos/demo-real-estate.css">
	<link rel="stylesheet" href="css/rtl.css">
	<script src="vendor/modernizr/modernizr.min.js"></script>
</head>
<body class="loading-overlay-showing" data-loading-overlay>
<div class="loading-overlay">
	<div class="bounce-loader">
		<div class="bounce1"></div>
		<div class="bounce2"></div>
		<div class="bounce3"></div>
	</div>
</div>
<div class="body">
	<header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 37, 'stickySetTop': '-37px', 'stickyChangeLogo': false}">
		<div class="header-body pt-none pb-none">
			<div class="header-container container custom-position-initial">
				<div class="header-row">
					<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
						<i class="fa fa-bars"></i>
					</button>
					<div class="row head_line">
						<div class="col-md-12">
							<div class="header-column logo_col">
								<div class="header-logo">
									<a href="index.php">
										<span class="logo_img">رفوف</span>
									</a>
								</div>
							</div>
							<div class="header-column qrcode_col">
								<img src="img/qrcode.png" class="qrcode_img" />
							</div>
							<div class="header-column">
								<div class="header-top header-top header-top-style-3 header-top-custom m-none hidden-xs">
									<div class="container">
										<nav class="header-nav-top pull-left">
											<ul class="nav nav-pills">
												<li class="hidden-xs"><a href="login.php">تسجيل دخول</a></li>
												<li class="hidden-xs"><a href="register.php">مستخدم جديد</a></li>
												<li class=""><a href="#">أعلن معنا</a></li>
											</ul>
										</nav>
									</div>
								</div>
								<div class="header-row">
									<div class="header-nav">
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse m-none">
											<nav>
												<ul class="nav nav-pills" id="mainNav">
													<li class="dropdown-full-color dropdown-quaternary active">
														<a href="index.php">
															الرئيسية
														</a>
													</li>
													<li class="dropdown-full-color dropdown-quaternary">
														<a href="cats.php">
															تصنيفات
														</a>
													</li>
													<li class="dropdown-full-color dropdown-quaternary">
														<a href="ads.php">
															العروض
														</a>
													</li>
													<li class="dropdown-full-color dropdown-quaternary">
														<a class="" href="companies.php">
															شركات
														</a>
													</li>
													<li class="dropdown-full-color dropdown-quaternary">
														<a href="favorites.php">
															المفضلة
														</a>
													</li>
													<li class="dropdown-full-color dropdown-quaternary">
														<a href="login.php">تسجيل الدخول</a>
													</li>
													<li class="dropdown-full-color dropdown-quaternary">
														<a href="contact.php">
															اتصل بنا
														</a>
													</li>
													<li class="dropdown-full-color dropdown-quaternary">
														<a href="about_us.php">
															من نحن
														</a>
													</li>
													<form action="ads.php" method="POST" class="navbar-form navbar-left">
														<div class="input-group">
															<input type="text" class="form-control" id="search_q" placeholder="ابحث في العروض والماركات">
															<span class="input-group-btn">
																<button type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
															</span>
														</div>
													</form>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row head_line hidden-lg">
						<div class="col-xs-3 col-md-2 text-center logo_wrapper">
							<div class="logo_flex"> </div>
							<img alt="Rofof" src="img/logo.png" class="logo_ad_img img-responsive" />
						</div>
						<div class="col-xs-9 col-md-10">
							<div class="ad_space">مساحة إعلانية</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div role="main" class="main">