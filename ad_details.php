<?php include('header.php'); ?>
	<section class="page-header page-header-light page-header-more-padding">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-12">
					<h1>خصم 20% على جميع أنواع العطور من محل هكاظ</h1>

					<div class="row text-center">
						<ul class="breadcrumb breadcrumb-valign-mid">
							<li class="active">تصنيف: <a href="cats.php">عطور وروائح</a></li>
						</ul>
					</div>
					<br>
					<div class="row">
						<a href="favorites.php" class="btn btn-default btn-sm btn-rd"><i class="fa fa-star text-yellow"></i> اضف إلى المفضلة</a>
						<a href="#qrcode" class="btn btn-default btn-sm btn-rd"><i class="fa fa-bars"></i> كود العرض</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container padd20">
		<div class="row pb-xl pt-md">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-7">

						<span class="thumb-info-listing-type thumb-info-listing-type-detail background-color-secondary text-uppercase text-color-light font-weight-semibold p-sm pl-md pr-md">
							خصم 35%
						</span>

						<div class="thumb-gallery">
							<div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">
								<div class="owl-carousel owl-theme manual thumb-gallery-detail show-nav-hover" id="thumbGalleryDetail">
									<?php for($i=1; $i<=4; $i++): ?>
									<div>
										<a href="img/offers/<?=$i?>.jpg">
											<span class="thumb-info thumb-info-centered-info thumb-info-no-borders font-size-xl">
												<span class="thumb-info-wrapper font-size-xl">
													<img src="img/offers/<?=$i?>.jpg" class="img-responsive">
													<span class="thumb-info-title font-size-xl">
														<span class="thumb-info-inner font-size-xl"><i class="icon-magnifier icons font-size-xl"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
									<?php endfor; ?>
								</div>
							</div>

							<div class="owl-carousel owl-theme manual thumb-gallery-thumbs mt" id="thumbGalleryThumbs">
								<?php for($i=1; $i<=4; $i++): ?>
								<div>
									<img alt="Offer details" src="img/offers/<?=$i?>.jpg" class="img-responsive cur-pointer">
								</div>
								<?php endfor; ?>
							</div>
						</div>

					</div>
					<div class="col-md-5">
						<a name="qrcode"></a>
						<table class="table table-striped">
							<colgroup>
								<col width="45%">
								<col width="55%">
							</colgroup>
							<tbody>
								<tr>
									<td class="background-color-primary text-light pt-md">
										السعر بعد الخصم
									</td>
									<td class="font-size-xl font-weight-bold pt-sm pb-sm background-color-primary text-light">
										30,59 SR
										<del class="price_before">45.10 SR</del>
									</td>
								</tr>
								<tr>
									<td>
										تصنيف
									</td>
									<td>
										عطور
									</td>
								</tr>
								<tr>
									<td>
										المدينة
									</td>
									<td>
										الرياض
									</td>
								</tr>
								<tr>
									<td>
										العنوان
									</td>
									<td>
										37 ش السعادة من شارع البحر - ميدان النصر
									</td>
								</tr>
								<tr>
									<td>
										QR Code
									</td>
									<td>
										<img src="img/qrcode2.png">
									</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h4 class="mt-md mb-md">نبذة عن العرض</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<legend class="other_ads_title">عروض أخرى في نفس القسم</legend>
						<div style="padding-left: 10px; padding-right: 10px;">
							<div class="owl-carousel other_prods_slider">
							<?php for($j=1; $j<10; $j++): ?>
								<div class="hp_prod">
									<a href="ad_details.php?id=<?=$j?>">
										<div class="cat_icon hp_prod_img"><img src="img/cats/<?=$i?>.jpg"></div>
										<div class="hp_price">40 SR</div>
									</a>
								</div>
							<?php endfor; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3" style="display: none;">
				<aside class="sidebar">
					<div class="agents text-color-light center">
						<h4 class="text-light pt-xl m-none">نبذة عن صاحب العرض</h4>
						<div class="owl-carousel owl-theme nav-bottom rounded-nav pl-xs pr-xs pt-md m-none" data-plugin-options="{'items': 1, 'loop': false, 'dots': false, 'nav': true}">
							<div class="pr-sm pl-sm">
								<a href="demo-real-estate-agents-detail.html" class="text-decoration-none">
									<span class="agent-thumb">
										<img class="img-responsive img-circle" src="img/icon.jpg" alt />
									</span>
									<span class="agent-infos text-light pt-md">
										<strong class="text-uppercase font-weight-bold">شركة السعادة</strong>
										<span class="font-weight-light">123-456-789</span>
										<span class="font-weight-light">bruno@domain.com</span>
									</span>
									<br>
								</a>
							</div>
						</div>
					</div>
				</aside>
			</div>
		</div>

		<div class="row" style="margin-bottom: 50px;">
			<div class="col-md-12">
				<div class="text-center">
					<a href="ads.php" class="btn btn-primary">العودة لقائمة العروض</a>
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php'); ?>
